package org.frknkrc44.afad;

import java.lang.reflect.Field;

public class Deprem {

	final public String tarih, saat, enlem,
			boylam, derinlik, rms, 
			tip, buyukluk, ulke, 
			il, ilce, koy, diger;
			
	public Deprem(String tarih, String saat, String enlem, String boylam,
				  String derinlik, String rms, String tip,
				  String buyukluk, String ulke, String il,
				  String ilce, String koy, String diger){

		this.tarih = tarih;
		this.saat = saat;
		this.enlem = enlem;
		this.boylam = boylam;
		this.derinlik = derinlik;
		this.rms = rms;
		this.tip = tip;
		this.buyukluk = buyukluk;
		this.ulke = ulke;
		this.il = il;
		this.ilce = ilce;
		this.koy = koy;
		this.diger = diger;
	}

    public Deprem(String tarih, String enlem, String boylam,
			String derinlik, String rms, String tip,
			String buyukluk, String ulke, String il,
			String ilce, String koy, String diger){

		String[] tarihSaat = tarih.split(" ");
		this.tarih = tarihSaat[0];
		this.saat = tarihSaat[1];
		this.saat = this.saat.substring(0,this.saat.length()-1);
		this.enlem = enlem;
		this.boylam = boylam;
		this.derinlik = derinlik;
		this.rms = rms;
		this.tip = tip;
		this.buyukluk = buyukluk;
		this.ulke = ulke;
		this.il = il;
		this.ilce = ilce;
		this.koy = koy;
		this.diger = diger;
	 }
	 
	 public String getLocation(){
		 if(diger.length() > 1){
			 return diger;
		 }
		 
		 String out = "";
		 if(koy.length() > 1){
			 out += koy + ", ";
		 }
		 
		 if(ilce.length() > 1){
			 out += ilce + ", ";
		 }
		 
		 if(il.length() > 1){
			 out += il + ", ";
		 }
		 
		 if(ulke.length() > 1){
			 out += ulke;
		 }
		 
		 return out;
	 }

	 @Override
	 public String toString() {
		 String out = Deprem.class.getSimpleName() + " {\n";
		 try {
			 Field[] fields = Deprem.class.getDeclaredFields();
			 for(Field field : fields){
				 out += "    " + field.getName() + " => " + field.get(this) + "\n";
			 }
		 } catch(Throwable t){}
		 out += "}";
		 return out;
	 }
}
