package org.frknkrc44.afad;

import java.util.ArrayList;

public class DepremList extends ArrayList<Deprem> {

	final private int days;
	private double minRange = 0.0;
	private String[] cityFilter;
	
	public DepremList(){
		this(1);
	}
	
	public DepremList(int days){
		this(days, false);
	}
	
	public DepremList(int days, boolean loadImmediately){
		this.days = days;
		if(loadImmediately){
			reload();
		}
	}
	
	public void setMinRange(double min){
		minRange = min;
	}
	
	public double getMinRange(){
		return minRange;
	}
	
	public void setCityFilter(String... cities){
		cityFilter = cities;
	}
	
	public void reload(){
		if(size() > 0){
			clear();
		}
		
		try {
			String raw = APIConnection.getRawData(days);
			String[] depremlerRaw = raw.split("\\\n");
			for(int i = 1;i < depremlerRaw.length;i++){
				String temp = depremlerRaw[i].trim();
				if(temp.length() > 0){
					String[] deprem = temp.split(",");
					String buyukluk = deprem[8];
					double buyuk = Double.parseDouble(buyukluk);
					if(buyuk < minRange){
						continue;
					}
					String tarih = deprem[2];
					String enlem = deprem[3];
					String boylam = deprem[4];
					String derinlik = deprem[5];
					String rms = deprem[6];
					String tip = deprem[7];
					String ulke = deprem[9];
					String il = deprem[10];
					String ilce = deprem[11];
					String koy = deprem[12];
					String diger = temp.substring(temp.lastIndexOf(koy)+koy.length(),temp.length());
					diger = diger.length() > 1 
								? diger.substring(1,diger.length()) 
								: "-";
					if(isInFilter(ulke, il, ilce, koy, diger)){
						add(new Deprem(tarih, enlem, boylam, derinlik, rms, tip,
										buyukluk, ulke, il, ilce, koy, diger));
					}
				}
			}
		} catch(Throwable t){}
	}
	
	private boolean isInFilter(String ulke, String il, String ilce, String koy, String diger){
		if(cityFilter == null){
			return true;
		}
		
		for(String filter : cityFilter){
			if(ulke.equals(filter) || il.equals(filter) ||
				ilce.equals(filter) || koy.equals(filter) || 
				diger.contains(filter)){
				return true;
			}
		}
		
		return false;
	}
	
}
