package org.frknkrc44.afad;

import java.io.*;
import java.net.*;

public class APIConnection {
    
    public static String getRawData(int days) throws IOException {
        URL url = new URL("https://deprem.afad.gov.tr/latestCatalogsExport");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

        String postData = parseValue("searchedMInput", 0);
        postData += parseValue("searchedUtcInput", 0);
        postData += parseValue("searchedLastDayInput", days);
        postData += parseValue("exportType", 2); // CSV

        postData = postData.substring(0, postData.length() - 1);

        byte[] postBytes = postData.getBytes();
        conn.setRequestProperty("Content-Length", String.valueOf(postBytes.length));

        conn.connect();

        OutputStream os = conn.getOutputStream();
        os.write(postBytes);
        os.close();

        InputStreamReader isr = new InputStreamReader(conn.getInputStream(), "ISO-8859-9");

        int read;
        char[] buffer = new char[1024];
        StringBuilder out = new StringBuilder();

        while ((read = isr.read(buffer, 0, buffer.length)) > 0) {
            out.append(buffer, 0, read);
        }
        
        isr.close();

        return out.toString();
    }

    private static String parseValue(String key, int value) {
        return key + "=" + String.valueOf(value) + "&";
    }

}
