package org.frknkrc44.kandilli;

public class Deprem {
	
	public final String tarih, saat, enlem, boylam, derinlik, siddet, konum;
	
	public Deprem(String tarih, String saat, String enlem, String boylam, String derinlik, String siddet, String konum){
		this.tarih = tarih;
		this.saat = saat;
		this.enlem = enlem;
		this.boylam = boylam;
		this.derinlik = derinlik;
		this.siddet = siddet;
		this.konum = konum;
	}
	
	public static Deprem getEmpty(){
		return new Deprem("","","","","","","");
	}

	@Override
	public String toString(){
		return tarih + " " + saat + " " + enlem + " " +
				boylam + " " + derinlik + " " + siddet + " " + konum;
	}
	
}
