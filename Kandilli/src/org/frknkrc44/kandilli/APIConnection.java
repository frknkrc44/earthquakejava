package org.frknkrc44.kandilli;

import java.io.InputStreamReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class APIConnection {
	
	private APIConnection(){}
	
	static String getRawData() {
		try {
			URL url = new URL("http://www.koeri.boun.edu.tr/scripts/LST1.asp");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.addRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) brave/0.7.10 Chrome/47.0.2526.110 Brave/0.36.5 Safari/537.36");
			conn.setDoInput(true);
			conn.connect();
			InputStreamReader dis = new InputStreamReader(conn.getInputStream(),"ISO-8859-9");
			char[] buf = new char[1024];
			int read;
			StringBuilder build = new StringBuilder();
			while((read = dis.read(buf,0,buf.length)) > 0){
				build.append(buf,0,read);
			}
			return build.toString();
		} catch(IOException e){
			return "";
		}
	}
	
}
