package org.frknkrc44.kandilli;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DepremList extends ArrayList<Deprem> {
	
	private List<String> mLocations;
	private double mMinRange = 0;
	
	public DepremList(){
		this(true);
	}
	
	public DepremList(boolean loadImmediately){
		if(loadImmediately){
			reload();
		}
	}
	
	public void setCityFilter(String... locations){
		if(locations == null){
			clearCityFilter();
			return;
		}
		
		if(mLocations == null){
			mLocations = new ArrayList<>();
		} else {
			mLocations.clear();
		}
		
		for(String loc : locations){
			mLocations.add(loc);
		}
	}
	
	public void clearCityFilter(){
		mLocations.clear();
		mLocations = null;
	}
	
	public void setMinRange(double minRange){
		mMinRange = minRange;
	}
	
	public void reload(){
		if(size() > 0){
			clear();
		}

		try {
			String raw = APIConnection.getRawData().replaceAll("\\r","");
			if(raw.contains("<pre>")){
				String pre = raw.substring(raw.indexOf("<pre>")+5,raw.indexOf("</pre>")).trim();
				String[] results = pre.split("\\\n");
				boolean lineFound = false;
				for(String result : results){
					if(lineFound && result.trim().length() > 0){
						String item = result.replaceAll(" +"," ").trim();
						String[] tmp = item.split(" ");
						String tarih = tmp[0];
						String saat = tmp[1];
						String enlem = tmp[2];
						String boylam = tmp[3];
						String derinlik = tmp[4];
						String siddet = tmp[6];

						String konum = item.substring(item.lastIndexOf(tmp[7])+tmp[7].length()+1);
						if(konum.endsWith("İlksel")){
							konum = konum.replace("İlksel","");
						} else {
							konum = konum.substring(0,konum.indexOf(tmp[tmp.length-3]));
						}

						addWithFilter(tarih,saat,enlem,boylam,derinlik,siddet,konum.trim());
					} else if(result.startsWith("---")){
						lineFound = true;
					}
				}
			}
		} catch(Throwable t){}
	}
	
	private void addWithFilter(String tarih,String saat,String enlem,String boylam,String derinlik,String siddet,String konum){
		if(mMinRange > Double.parseDouble(siddet) || mLocations != null && !mLocations.contains(parseLocation(konum.trim()))){
			return;
		}
		
		add(new Deprem(tarih,saat,enlem,boylam,derinlik,siddet,konum.trim()));
	}
	
	private String parseLocation(String konum){
		if(konum.contains("(")){
			return konum.substring(konum.lastIndexOf("(")+1,konum.length()-1);
		} else if(konum.contains("-")){
			return konum.substring(konum.lastIndexOf("-")+1);
		}
		return konum;
	}

	@Override
	public Deprem[] toArray(){
		Deprem[] out = new Deprem[size()];
		if(size() > 0){
			for(int i = 0;i < size();i++){
				out[i] = get(i);
			}
		}
		return out;
	}
	
}
